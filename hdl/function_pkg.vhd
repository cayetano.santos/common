-- * header
-------------------------------------------------------------------------------
-- Title      : function package
-- Project    : common
-------------------------------------------------------------------------------
-- File       : function_pkg.vhd
-- Author     : Jose Correcher  <jose.correcher@gmail.com>
-- Company    :
-- Created    : 2020-05-27
-- Last update: 2020-07-15
-- Platform   :
-- Standard   : VHDL'08
-------------------------------------------------------------------------------
-- Description: This package contains VHDL generic functions that can be used
--              in different rtl projects.
-------------------------------------------------------------------------------
-- Copyright (c) 2020
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2020-05-27  1.0      jcorrecher Created
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- * libraries
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-------------------------------------------------------------------------------
-- * package declaration
-------------------------------------------------------------------------------

package function_pkg is

  function f_b2g (signal din : unsigned) return unsigned;
  function f_g2b (signal din : unsigned) return unsigned;
  function f_zxt(arg  : std_logic_vector;
                 size : integer) return std_logic_vector;

end package function_pkg;

-------------------------------------------------------------------------------
-- * package body
-------------------------------------------------------------------------------

package body function_pkg is

-------------------------------------------------------------------------------
-- * conversion
-------------------------------------------------------------------------------

  -- ** convert binary to gray
  function f_b2g (signal din : unsigned) return unsigned is
    variable bin2g : unsigned(din'range);
  begin  -- b2g
    bin2g(din'high) := din(din'high);
    bin2gray :
    for i in 0 to din'length-2 loop
      bin2g(i) := din(i+1) xor din(i);
    end loop;
    return (bin2g);
  end f_b2g;

  -- ** convert gray to binary
  function f_g2b (signal din : unsigned) return unsigned is
    variable g2bin : unsigned(din'range);
  begin  -- g2b
    g2bin(din'high) := din(din'high);
    gray2bin :
    for i in din'length-2 downto 0 loop
      g2bin(i) := g2bin(i+1) xor din(i);
    end loop;
    return (g2bin);
  end f_g2b;

  -- ** right zero extension
  function f_zxt (arg  : std_logic_vector;
                  size : integer) return std_logic_vector is
    variable result : unsigned(size-1 downto 0) := (others => '0');
  begin
    -- result(arg'range) := unsigned(arg);                -- option a.
    result            := resize(unsigned(arg), size);  -- option b.
    result            := shift_left(result, size-arg'length);
    return std_logic_vector(result);
  end f_zxt;

end package body function_pkg;
